﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadWriteExcel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
                  
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = open.FileName;
                DataTable dt = new DataTable();
                dt = Excel.Worksheets(textBox1.Text);
                listBox1.DataSource = dt;
     
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = Excel.Import(textBox1.Text, 3, 1, true, listBox1.SelectedIndex);
            dataGridView1.DataSource = dt;
        }
    }
}
